#include "myserver.h"




void myserver::startServer() // запускаем сервер
{
    if (this->listen(QHostAddress::Any,5555)) // метод листен заупскает сервер с 2-мя параметрами ( адрес и порт)
    {
        db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("D:\\Something\\Server\\workers.db");
        if(db.open())
        {
            qDebug()<<"Listenning and open DB"; // при успехе
        }
        else
        {
            qDebug()<<"DB not open";

        }

    }
    else {
        qDebug()<<"Not Listenning";
    }
}


void myserver::incomingConnection(int socketDescriptor) // в случае нового соединения
{
    socket = new QTcpSocket(this); // создаем сокет для общения
    socket->setSocketDescriptor(socketDescriptor); // присваиваем уникальный номер

connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady())); // связь обьекта сокет со слотом sockReady
connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));// связь обьекта сокет со слотом sockDisc

qDebug()<<socketDescriptor<<"client connected"; // выводим сокет клиента




socket->write("{\"type\":\"connect\",\"status\":\"yes\"}");//отправляем на клиент текст формата json
qDebug()<<"Send a client connect status - YES";
}

void myserver::sockReady()
{
            Data= socket->readAll();// считываем сообщение от клиента
            qDebug()<<"Data"<<Data;

            doc = QJsonDocument::fromJson(Data,&docError);//помещяем его в переменную doc типа QJsonDocument

                    if(docError.errorString()=="no error occurred")//если нет ошибок
                {
                        if(doc.object().value("type").toString() == "select" && (doc.object().value("params").toString()== "workers"))
                        {
//                            QFile file;
//                            file.setFileName("D:\\Something\\Server\\workers.json");
//                            if(file.open(QIODevice::ReadOnly| QFile::Text))
//                            {
//                                QByteArray fromFile = file.readAll();
//                                QByteArray itog = "{\"type\":\"resultSelect\",\"result\":" +fromFile + "}";

//                                socket->write(itog);
//                                socket->waitForBytesWritten(500);
//                            }
//                            file.close();

                            if(db.isOpen())
                            {
                              QByteArray itog = "{\"type\":\"resultSelect\",\"result\":[";
                              QSqlQuery* query = new QSqlQuery(db);
                             if(  query->exec("SELECT name FROM listworkers"))
                             {
                                        while(query->next())
                                        {
                                            itog.append("{\"name\":\""+query->value(0).toString()+ "\"},");
                                        }
                                        itog.remove(itog.length()-1,1);
                                        itog.append("]}");
                                        socket->write(itog);
                                        socket->waitForBytesWritten(500);
                             }
                             else
                             {
                                    qDebug()<<"Not success";
                             }

                            }

                        }
                }
}



void myserver::sockDisc() // в случае дисконекта
{
    qDebug()<<"Disconect";
    socket->deleteLater(); // приложение отслеживает состояние и само удаляет при удалении
}

