#ifndef MYSERVER_H
#define MYSERVER_H
#include <QTcpServer> // необходимо для работы по сети
#include <QTcpSocket>// необходимо для работы по сети
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlQuery>
class myserver: public QTcpServer // класс наследник от QTcpServer
{
     Q_OBJECT // необходимо для сигналов и слотов
public:

    QTcpSocket* socket; // отвечает за сокеты
    QByteArray Data; // складываем данные для передачи
    QJsonDocument doc;
    QJsonParseError docError;
    QSqlDatabase db;


public slots:
    void startServer(); //запускает сервер
    void incomingConnection(int socketDescriptor); // метод по умолчанию (описание действий при подключаении)
    void sockReady(); // готовность читать полученные данные
    void sockDisc(); // при отключение удаление обьекта

};





#endif // MYSERVER_H
