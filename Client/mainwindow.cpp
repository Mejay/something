#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) : // что будет происходить сразу при запуске
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

socket = new QTcpSocket(this);// создаем сокет
connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady())); // связь между сокетом и слотом sockReady
connect(socket,SIGNAL(disconected()),this,SLOT(sockDisc()));// связь между сокетом и слотом  sockDisc

}


MainWindow::~MainWindow()
    {
        delete ui;
    }


void MainWindow::on_pushButton_clicked() // при нажатии кнопки (подключиться)
{
    socket->connectToHost("127.0.0.1",5555); // соеденемся с сервером

}
void MainWindow::sockDisc()// при дисконекте
{
    socket->deleteLater(); // автоматическое удаление
}
void MainWindow::sockReady()
{
if (socket->waitForConnected(500)) // если соединение устойчивое
    {
    socket->waitForConnected(500);// ждем готовности сокета на чтение (пол секунды)
    Data = socket->readAll(); // читает все содержимое сокета и записывает в Data

    qDebug()<<Data; // выводдим то,что передает сервер
    doc = QJsonDocument::fromJson(Data,&docError);// записываем в doc полученное сообщение и указываем переменную для ошибок
        if(docError.errorString()=="no error occurred")//если нет ошибок//Если нет ошибок то разбираем сообщение по частям
        {
            if(doc.object().value("type").toString()=="connect" && (doc.object().value("status").toString()=="yes"))//если значение полей п
            {// правильное,то выводим
                QMessageBox::information(this,"Информация","Соединение установлено");
            }
            else if(doc.object().value("type").toString()== "resultSelect")// если получаем такой тип
            {
                QStandardItemModel* model = new QStandardItemModel(nullptr);// создаем моделбку
                model->setHorizontalHeaderLabels(QStringList()<<"name");//добавляем название таблицы Name

                QJsonArray docAr = doc.object().value("result").toArray();// считываем в массив значение result
                for (int i=0 ; i < docAr.count();i++)// перебор массива
                {
                    QStandardItem* col = new QStandardItem(docAr[i].toObject().value("name").toString());//записываем каждый result в col
                    model->appendRow(col);// запихиваем каждый col в модель
                }
                ui->tableView->setModel(model);// записываем модель в таблицу

            }
            else
            {
                QMessageBox::information(this,"Информация","Соединение не установлено");
            }
        }
        else
        {
            QMessageBox::information(this,"Информация","Ошибки с форматом передачи данных"+ docError.errorString());
        }
    }
}




void MainWindow::on_pushButton_2_clicked()
{
    if(socket->isOpen())
    {
        socket->write("{\"type\":\"select\",\"params\":\"workers\"}");
        socket->waitForBytesWritten(500);
    }
    else
    {
       QMessageBox::information(this,"Информация","Не установленно соединение");
    }
}
